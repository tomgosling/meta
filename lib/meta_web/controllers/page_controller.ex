defmodule MetaWeb.PageController do
  use MetaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
